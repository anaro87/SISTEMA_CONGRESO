<?php 

class ExtDocsController extends BaseController {
	
	public function index()
	{
		if (Auth::guest()) return Redirect::guest('login');
		$extensiones = ExtensionDocumento::orderBy('nomExtension')->get();
		$this->layout->content = View::make('adminExtDocs')->with('extensiones',$extensiones);
	}

	public function create()
	{
		if(Request::ajax())
		{
			try {
				$validRecord = ExtensionDocumento::where("tipoExtension", "=", Input::get('extension'))
								->count();
				if($validRecord > 0)
				{
					return Response::json(array('error' => True, 'mensaje' => 'Extensi&oacute;n ya existente, imposible agregar'));
				}					
				$miModelo = new ExtensionDocumento;
				$miModelo->nomExtension	= Input::get('nomExtension');
				$miModelo->tipoExtension 	= Input::get('extension');
				$miModelo->save();
				return Response::json(array('error' => False, 'mensaje' => 'Guardando... espere un momento mientras se refresca la pantalla'));
			} catch (Exception $e) {
				return Response::json(array('error' => True, 'mensaje' => 'Server error, oroblemas para agregar Extensi&oacute;n'));
			}
		}
	}

	public function edit($id)
	{
		if(Request::ajax())
		{
			if(Input::get('accion') == 'edit')
			{
				try {

					$validRecord = ExtensionDocumento::where("tipoExtension", "=", Input::get('extension'))
									->where("idExtensionDocumento", "<>", $id)
									->count();
					if($validRecord > 0)
					{
						return Response::json(array('error' => True, 'mensaje' => 'Extensi&oacute;n ya existente, no se puede modificar'));
					}

					$miModelo	= ExtensionDocumento::find($id);
					$miModelo->nomExtension	= Input::get('nomExtension');
					$miModelo->tipoExtension 	= Input::get('extension');

					if (!$miModelo->save()) {
						return Response::json(array('error' => True, 'mensaje' => 'Problemas para modificar extensi&oacute;n'));
					}
					return Response::json(array('error' => False, 'mensaje' => 'Actualizando... espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'Server error, problemas para modificar extensi&oacute;n'));
				}
			}
			
			if(Input::get('accion') == 'delete')
			{
				try {
					$currentId = $id;
					ExtensionDocumento::destroy($currentId);
					return Response::json(array('error' => False, 'mensaje' => 'Extensi&oacute;n Eliminada, espere un momento mientras se refresca la pantalla'));
				} catch (Exception $e) {
					return Response::json(array('error' => True, 'mensaje' => 'ALTO! Extensi&oacute;n en uso, no se puede eliminar'));
				}
			}
			
		}
	}
}