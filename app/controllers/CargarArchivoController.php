<?php

//use BaseController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Mgallegos\LaravelJqgrid\Encoders\RequestedDataInterface;

class CargarArchivoController extends BaseController 
{
    function cargarArchivo()
    {
            ini_set("memory_limit",-1); 
            $fileElement = Input::get('fileName');
            
            $accion =  Input::get('ac');
            $idTabla = Input::get('i');//solo se usa cuando son updates del file
            $tabla = Input::get('wh');//tabla a donde se debe de meter este url
            $tipoArchivo = Input::get('ty');
            $carpeta = "";

            if(isset($_FILES[$fileElement]))
            {
                switch ($tabla) 
                {
                    case 'pa':
                            $carpeta = "paper";
                    break;

                    case 'pre':
                            $carpeta = "presentaciones";
                    break;

                    case 'dc':
                            $carpeta = "detalle_congreso";
                    break;

                    case 'axf':
                            $carpeta = "autor_x_ficha";
                    break;
                    
                    default:
                        # code...
                    break;
                }
                if (!file_exists(public_path()."/archivos/$carpeta/")) {
                    mkdir(public_path()."/archivos/$carpeta/", 0777, true);
                }
                $uploaddir = public_path() . "/archivos/$carpeta/";

                $realName = $_FILES[$fileElement]['name'];
                $peso = $_FILES[$fileElement]['size'];
                if($peso > 1048557690909000000)
                {
                    return Response::json('error: El archivo sobre pasa el tamaño de 10MB permitido.', 400);
                }
                else
                {
                    $nomExt = explode(".",$_FILES[$fileElement]['name']);

                    $indice = count($nomExt)-1;
                    if($accion == "in")
                    {

                        $newName = "0_CONIA_$idTabla.".$nomExt[$indice];
                    }
                    else
                    {
                        //CAMBIAR ESTO POR QUERY DE LARAVEL
                        //$sql="SELECT RUTA_FOTO FROM SG_USUARIO_PEC WHID_USUARIO=$idUsuario";
                        $nomFo = "nombreFoto";//$lnk->valueQuery($sql);
                        $arr = Array();
                        $arr = explode("_",$nomFo);
                        $i = (int)$arr[1]+1;
                        $newName = $i."_CONIA_$idTabla.".$nomExt[$indice];
                        unlink($nomFo); 
                    }//else accion
                    $filepath = $uploaddir.$newName;
 
                    if(file_exists($filepath))
                    {
                        unlink($filepath);
                    }
                        
                    if($tipoArchivo == "fo")
                    {
                        $pattern = "/\.(img|jpg|gif|png|jpeg)$/i";
                    }
                    else
                    {
                        $pattern = "/\.(pdf|doc|docx|txt|rtf|xls|xlsx|odt|ods|ppt|pps|odp|img|jpg|gif|png|rar|zip|jpeg)$/i";
                    }
                    if(preg_match($pattern,$filepath) == null)
                    {
                        return Response::json('error 0', 400);
                    }
                    else if($_FILES[$fileElement]["error"] == 0)
                    {
                        if(!move_uploaded_file($_FILES[$fileElement]['tmp_name'],$filepath))
                        {
                            return Response::json('error: No se logro subir el archivo.', 400);
                        }
                        else
                        {
                            switch ($tabla) 
                            {
                                case 'pa':
                                    $paper = new paper();
                                    $paper->updateRuta($idTabla, $filepath);    
                                break;

                                case 'pre':
                                    $carpeta = "presentaciones";
                                    $presentacion = new Presentacion();
                                    $presentacion->updateRuta($idTabla, $filepath);
                                break;

                                case 'dc':
                                        $carpeta = "detalle_congreso";
                                        $detalleCongreso = new detalleCongreso();
                                        $detalleCongreso->updateRuta($idTabla, $filepath);
                                break;
                                
                                case 'axf':
                                        $carpeta = "autor_x_ficha";
                                        $autoXfic = new AutorXFicha();
                                        $autoXfic->updateRuta($idTabla, $filepath);
                                break;
                                
                                default:
                                    # code...
                                break;
                            }

                        }

                    } 
                    else
                    {
                        return Response::json('Hubo un problema al subir el archivo en el servidor. 0', 400);
                    }

                }//else peso  
            }     

    }//fin funcion
     
}