<?php

class EmailController extends BaseController {
/*
	Basicamente para hacer uso del envio de emails, se ha construido una clase que permite el envio de parametros atraves de un array 
	en este caso solo para terminos demostrativos tenemos en la primera fila todo lo relacionado con el envio del correo
	el destinatario, quien lo envia nombre y correo.
	
	En la segunda linea (16) se indica que vista o plantilla se usara en el correo .. en este caso es un archivo html que se puede encontrar
	en la carpeta emails.auth, aqui se puede poner HTML a secas que es lo que se pretende hacer cuando se saquen las plantillas de la base

	y el resto son parametros que se utilizan dentro de la vista.
		
*/
	public function enviar()
	{

		$data = array( 'email' => '00101809@uca.edu.sv', 'first_name' => 'Administrador', 'from' => 'congresos.uca@gmail.com', //Información destinatario/Remitente
						'plantilla' => 'emails.auth.normal', //aqui se indica la plantilla de correo a utilizar
						'usuario'=>'Usuario de CONUCA', //Parametro para la vista el nombre que aparece en el saludo
						'mensaje'=>'Deseamos informarle que usted ha sido elegido para participar en el congreso CONIA2014 
									efectuando el rol de Revisor del mismo, si usted esta de acuerdo en continuar el proceso 
									favor clickear el link que se adjunta en este correo y completar los pasos que ahi se indican.'); //parametro cuerpo del mensaje

		Correo::enviarCorreos($data);
		var_dump("Mensaje Enviado");
	}
}
