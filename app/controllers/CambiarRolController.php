<?php
class CambiarRolController extends \BaseController {

	/**
	 * Cambia el rol a rol de chair.
	 *
	 * @return Json Siempre es una respuesta sin error ya que se valida en el basecontroller poder acceder a esta funcion.
	 */
	public function chair()
	{
		if (Request::ajax())
		{
			Session::put('rol_barra', 'Chair');
			return Response::json(array('error' => False, 'mensaje' => "Se ha cambiado el rol acutal."));
		}
	}

	/**
	 * Cambia el rol a rol de pc.
	 *
	 * @return Json Siempre es una respuesta sin error ya que se valida en el basecontroller poder acceder a esta funcion.
	 */
	public function pc()
	{
		if (Request::ajax())
		{		
			Session::put('rol_barra', 'PC');
			return Response::json(array('error' => False, 'mensaje' => "Se ha cambiado el rol acutal."));
		}
	}

	/**
	 * Cambia el rol a rol de revisor.
	 *
	 * @return Json Siempre es una respuesta sin error ya que se valida en el basecontroller poder acceder a esta funcion.
	 */
	public function revisor()
	{
		if (Request::ajax())
		{
			Session::put('rol_barra', 'Revisor');
			return Response::json(array('error' => False, 'mensaje' => "Se ha cambiado el rol acutal."));
		}
	}

	/**
	 * Cambia el rol a rol de autor.
	 *
	 * @return Json Siempre es una respuesta sin error ya que se valida en el basecontroller poder acceder a esta funcion.
	 */
	public function autor()
	{
		if (Request::ajax())
		{
			Session::put('rol_barra', 'Autor');
			return Response::json(array('error' => False, 'mensaje' => "Se ha cambiado el rol acutal."));
		}
	}
}