<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
	$debug = Config::get('app.debug');
	if($debug)
	{
		Log::info('Inicio del Request=========================================>');
	}
});


App::after(function($request, $response)
{
	//
	$debug = Config::get('app.debug');
	if($debug)
	{
		Log::info('Querys del request');
		foreach(DB::getQueryLog() as $query) Log::info($query);
		Log::info('Fin del Request============================================>');
	}
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::action('LoginController@index');
	else{
		if (Session::getId() != Auth::user()->last_session)
		{
			//Session::flush();
			Auth::logout();
			Session::put('accessError', 'true');
			return Redirect::action('LoginController@index');
		}
	}
});

Route::filter('authJSON', function()
{
	if (Auth::guest()) return Response::json(array('error' => True, 'mensaje' => 'Usted necesita iniciar sesión primero, Redireccionando a login ...', 'errorAuthJSON' => True));
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::action('HomeController@inicio');
});

Route::filter('guestJSON', function()
{
	if (Auth::check()) return Response::json(array('error' => True, 'mensaje' => 'Se ha iniciado sesión! No se puede acceder hasta hacer logout'));
});


/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
