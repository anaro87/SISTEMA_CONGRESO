<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSesionTable extends Migration {

	public function up()
	{
		Schema::create('sesion', function(Blueprint $table) {
			$table->increments('idSesion');
			$table->timestamp('fecSesion');
			$table->integer('duracionSesion');
			$table->string('moderador');
			$table->time('horaInicio');
			$table->time('horaFin');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
			$table->integer('idTematica')->unsigned();
			$table->integer('idCongreso')->unsigned();
			$table->string('nombreSesion');
		});
	}

	public function down()
	{
		Schema::drop('sesion');
	}
}
