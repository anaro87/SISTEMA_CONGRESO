<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolTable extends Migration {

	public function up()
	{
		Schema::create('rol', function(Blueprint $table) {
			$table->increments('idRol');
			$table->string('nomRol');
		});
	}

	public function down()
	{
		Schema::drop('rol');
	}
}