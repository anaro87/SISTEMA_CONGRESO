<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAulaTable extends Migration {

	public function up()
	{
		Schema::create('aula', function(Blueprint $table) {
			$table->increments('idAula');
			$table->integer('idDetalleCongreso')->unsigned();
			$table->string('nomAula');
			$table->string('tipoAula');
		});
	}

	public function down()
	{
		Schema::drop('aula');
	}
}