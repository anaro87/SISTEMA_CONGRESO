<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsuarioXAreainvestigacionTable extends Migration {

	public function up()
	{
		Schema::create('usuario_x_areainvestigacion', function(Blueprint $table) {
			$table->increments('idUsuarioXAreainvestigacion');
			$table->integer('idUsuario')->unsigned();
			$table->integer('idAreaInvestigacion')->unsigned();
			$table->text('ResumenUsuario');
			$table->timestamp('fecModUAI');
		});
	}

	public function down()
	{
		Schema::drop('usuario_x_areainvestigacion');
	}
}