<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePcXTematicaTable extends Migration {

	public function up()
	{
		Schema::create('pc_x_tematica', function(Blueprint $table) {
			$table->increments('idPcTematica');
			$table->integer('idUsuariorolXCongreso')->unsigned();
			$table->integer('idTematica')->unsigned();
			$table->timestamps();
			$table->unique(array('idUsuariorolXCongreso', 'idTematica'));
		});
	}

	public function down()
	{
		Schema::drop('pc_x_tematica');
	}
}