<?php
class CategoriaTableSeeder extends Seeder {

	public function run()
	{
		DB::table('categoria')->delete();
		Categoria::create(['nomCategoria' => 'Conferencia magistral']);
		Categoria::create(['nomCategoria' => 'Ponencia']);
		Categoria::create(['nomCategoria' => 'Charla Libre']);
	}
}
