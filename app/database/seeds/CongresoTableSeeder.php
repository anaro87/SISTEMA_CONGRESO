<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CongresoTableSeeder extends Seeder {

	public function run()
	{
		DB::table('congreso')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);

		/*$debug = Config::get('app.debug');
		if($debug)
		{
				Congreso::create([
				'nomCongreso' => 'Congreso 1' . $faker->word,
				'acronimoCongreso' => $faker->word,
				'webCongreso' => $faker->url,
				'ciudad' => 'San Salvador',
				'pais' => 'El Salvador',
				'fecIniCongreso' => date('Y-m-d H:i:s'),
				'fecFinCongreso' => date('Y-m-d H:i:s'),
				'fecModCongreso' => date('Y-m-d H:i:s'),
				'emailOrgCongreso' => $faker->companyEmail ,
				'comentarioCongreso' => $faker->text ,
				'idEstadoCongreso' => EstadoCongreso::all()->random()->idEstadoCongreso,
				'idCreador' => Usuario::all()->random()->idUsuario
			]);
			
			Congreso::create([
					'nomCongreso' => 'Congreso 2' . $faker->word,
					'acronimoCongreso' => $faker->word,
					'webCongreso' => $faker->url,
					'ciudad' => 'San Salvador',
					'pais' => 'El Salvador',
					'fecIniCongreso' => date('Y-m-d H:i:s'),
					'fecFinCongreso' => date('Y-m-d H:i:s'),
					'fecModCongreso' => date('Y-m-d H:i:s'),
					'emailOrgCongreso' => $faker->companyEmail ,
					'comentarioCongreso' => $faker->text ,
					'idEstadoCongreso' => EstadoCongreso::all()->random()->idEstadoCongreso,
					'idCreador' => Usuario::all()->random()->idUsuario
			]);
			

			Congreso::create([
					'nomCongreso' => 'Congreso Prueba',
					'acronimoCongreso' => 'Conpu',
					'webCongreso' => 'www.congreso.com',
					'ciudad' => 'San Salvador',
					'pais' => 'El Salvador',
					'fecIniCongreso' => date('Y-m-d H:i:s'),
					'fecFinCongreso' => date('Y-m-d H:i:s'),
					'fecModCongreso' => date('Y-m-d H:i:s'),
					'emailOrgCongreso' => 'Congreso.uca.edu.sv' ,
					'comentarioCongreso' => 'Este es un comentario de prueba' ,
					'idEstadoCongreso' => 2,
					'idCreador' => 2
			]);

		}*/
	}

}
