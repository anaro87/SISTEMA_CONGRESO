<?php
// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class AgendaTableSeeder extends Seeder 
{

	public function run()
	{
		DB::table('agenda')->delete();
		$faker = Faker::create('es_MX');
		$faker->seed(1234);

		//DB::table('agenda')->truncate();
		//'idAula' => Aula::all()->random()->idAula,

		foreach(range(1,1) as $index)
		{
			Agenda::create
			([
				'titulo' => $faker->word,
				'fecha' => date('Y-m-d H:i:s'),
				'horaInicio' => "0:00",
				'horaFin' => "0:00",
				'idCongreso' => 0,
				'idTematica' => 0,
				'idSesion' => 0,
				'aulaLabel' => $faker->word,
				'idElementoHtml' => $faker->word,
				'esActividad' => false
			]);
		}
	}

}
