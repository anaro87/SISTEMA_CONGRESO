@section('content')
<div class="col-sm-10 col-sm-offset-1" style="margin-top: 30px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Env&iacute;o de correo</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-10 col-sm-offset-1">
					<form role="form" class="form-horizontal" name="formCorreo" >
						<br>
						<div class="alert alert-info fade in">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							<strong>Sugerencia</strong>
							<br/><br/>
							<strong>1) </strong> 
							Puede utilizar las siguientes etiquetas para darle formato a su correo: <br>
							<b>[b]</b>'texto'<b>[/b]</b> para poner texto en <b>negrita</b>,<br>
							<b>[i]</b>'texto'<b>[/i]</b> para poner texto en <i>cursiva</i>,<br>
							<b>[u]</b>'texto'<b>[/u]</b> para poner texto <u>subrayado</u>,<br>
							<b>[t]</b>'texto'<b>[/t]</b> para poner texto como t&iacute;tulo.
							<br/><br/>
							<strong>2) </strong> 
							Puede utilizar las siguientes etiquetas para personalizar su mensaje: <br>
							<b>[*PNombre*]</b> para poner el primer nombre,<br>
							<b>[*PApellido*]</b> para poner el primer apellido,<br>
							<b>[*Nombre_Completo*]</b> para poner el nombre completo.
						</div>
						<fieldset>
							<legend>Informaci&oacute;n del mensaje</legend>
							<div class="form-group">
								<label for="remitente" class="col-sm-3 control-label">De*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="100" class="form-control" id="remitente" name="remitente" value="{{$remitente->nombreUsuario}}&nbsp;{{$remitente->apelUsuario}}&nbsp;({{$remitente->emailUsuario}})" />
								</div>
							</div>
							<div class="form-group">
							<label for="destinatario" class="col-sm-3 control-label">Para*:</label>
							<div class="col-sm-9">
								<select multiple data-placeholder="Seleccione los destinatarios" style="width:100%" class="chosen" id="destinatarios" size="5">
							 		@foreach ($usuarios as $usuario)
							 		<option value="{{$usuario->idUsuario}}"  name="destinatario">{{$usuario->nombreUsuario}}&nbsp;{{$usuario->apelUsuario}}&nbsp;({{$usuario->emailUsuario}})</option>
							 		@endforeach
								</select>
							</div>
							</div>
							<div class="form-group">
								<label for="asunto" class="col-sm-3 control-label">Asunto*:</label>
								<div class="col-sm-9">
									<input type="text" maxlength="100" class="form-control" id="asunto" name="asunto" placeholder="Ingrese el asunto del mensaje" />
								</div>
							</div>
							<div class="form-group">
								<label for="cuerpo" class="col-sm-3 control-label">Mensaje*:</label>
								<div class="col-sm-9">
									<textarea class="form-control" id="cuerpo" name="cuerpo" placeholder="Ingrese el cuerpo del mensaje" rows="10" ></textarea>
								</div>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
					<button type="button" id="btnEnviar" class="btn btn-primary btn-default">Enviar Mensaje</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">

	$(document).ready(function()
	{
		$("#destinatarios").chosen({max_selected_options: 100});
		$("#remitente").prop('disabled', true);

		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('HomeController@inicio')}}";
		});

		$("#btnEnviar").click(function()
		{

			var remitente = $("[name=remitente]").val();
			var destinatarios = $("#destinatarios").val() || [];
			var asunto = $("[name=asunto]").val();
			var cuerpo = $("[name=cuerpo]").val();
			var error = false;

			if( remitente.length == 0 )
			{
				alertify.error("El mensaje debe poseer al menos un remitente.");
				$("#remitente").parent().removeClass('has-error').addClass('has-error');
				error = true;
				var btn=this;
				btn.innerHTML='Enviar Solicitud';
				btn.disabled=false;
			}
			else
			{
				$("#remitente").parent().removeClass('has-error');
			}


			if( remitente.length > 100 )
			{
				alertify.error("El remitente es inválido. Por favor cierre sesión y vuelva a iniciar.");
				$("#remitente").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("#remitente").parent().removeClass('has-error');
			}

			if( destinatarios.length == 0 )
			{
				alertify.error("El mensaje debe poseer al menos un destinatario.");
				$("#invest").parent().removeClass('has-error').addClass('has-error');
				error = true;
				var btn=this;
				btn.innerHTML='Enviar Solicitud';
				btn.disabled=false;
			}
			else
			{
				$("#invest").parent().removeClass('has-error');
			}


			if( destinatarios.length > 100 )
			{
				alertify.error("El mensaje no debe poseer más de 100 destinatarios.");
				$("#invest").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}
			else
			{
				$("#invest").parent().removeClass('has-error');
			}

			if(asunto.length < 1)
			{
				alertify.error("El asunto del mensaje es obligatorio.");
				$("[name='asunto']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{

				$("[name='asunto']").parent().removeClass('has-error');	
			}

			if(asunto.length > 100)
			{
				alertify.error("El asunto del mensaje es demasiado extenso.");
				$("[name='asunto']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			else
			{

				$("[name='asunto']").parent().removeClass('has-error');	
			}
			
			if(cuerpo.length < 1){
				alertify.error("El cuerpo del mensaje es obligatoria.");
				$("[name='cuerpo']").parent().removeClass('has-error').addClass('has-error');
				error = true;
			}else{
				$("[name='cuerpo']").parent().removeClass('has-error');
			}

			if(cuerpo.length > 1000 )
			{
				alertify.error("El cuerpo del mensaje es demasiado extenso.");
				$("[name=cuerpo]").parent().removeClass('has-error').addClass('has-error');
				return;
			}
			else
			{
				$("[name=cuerpo]").parent().removeClass('has-error');
			}
			if(!error)
			{

				$.post("{{URL::action('CorreosController@adminSendMail')}}", {asunto: asunto, destinatarios: destinatarios, cuerpo: cuerpo, remitente: remitente})
				.done(function(data, status,jqXHR)
				{
					
					if(data.error)
					{
						alertify.error(data.mensaje);
						
					}
					else
					{
						alertify.success(data.mensaje);

						document.forms["formCorreo"].submit();				
					}
				})
				.fail(function(data, status,jqXHR)
				{
					
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor.");
				});
			}
			else{
				
			}
		});
	});
</script>
@stop