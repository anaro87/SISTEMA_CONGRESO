@section('content')

<br>
<?php $Usuario = Usuario::find($idUsuario);  ?>
{{ Form::model($Usuario,  ['id' => 'formEdicion','method' => 'POST' , 'url'=>URL::action('AdminusuariosController@update',array($idUsuario) ) ]) }}
<div class="panel-body">
	@if (Session::has('message'))
	<input type='hidden' name='errmessage' id='errmessage' value="{{ Session::get('message') }}"></input>
	@endif
	
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h3 class="text-center">
				Edici&oacute;n de Usuario
			</h3>
		</div>
	</div>
	
	<br/>
	<div class="row clearfix">
		<div class="col-md-2 column">
			<label class="col-sm-3 control-label">Nombres*:&nbsp;</label>
		</div>
		<div class="col-md-4 column">
			{{ Form::text('nombreUsuario') }}
		</div>
		<div class="col-md-6 column"></div>
	</div>
	<br/>
	<div class="row clearfix">
		<div class="col-md-2 column">
			<label class="col-sm-3 control-label">Apellidos*:&nbsp;</label>
		</div>
		<div class="col-md-4 column">
			{{ Form::text('apelUsuario') }}
		</div>
		<div class="col-md-6 column"></div>
	</div>
	<br/>
	<div class="row clearfix">
		<div class="col-md-2 column">
			<label class="col-sm-3 control-label">Contrase&ntilde;a*:&nbsp;</label>
		</div>
		<div class="col-md-4 column">
			No cambiar contrase&ntilde;a {{ Form::checkbox('nocambiar', '1', false); }}
			{{ Form::password('passwordUsuario') }}
		</div>
		<div class="col-md-6 column"></div>
	</div>
	<br/>
	<div class="row clearfix">
		<div class="col-md-2 column">
			<label class="col-sm-3 control-label">Confirmar Contrase&ntilde;a*:&nbsp;</label>
		</div>
		<div class="col-md-4 column">
			<input name="confirmPassword" class="form-control" type="password" ></input>
		</div>
		<div class="col-md-6 column"></div>
	</div>
	<br/>
	
	<div class="row clearfix">
		<div class="col-md-2 column">
			<label class="col-sm-3 control-label">Correo Electr&oacute;nico*:&nbsp;</label>
		</div>
		<div class="col-md-4 column">
			{{ Form::email('emailUsuario') }}
		</div>
		<div class="col-md-6 column"></div>
	</div>
	<br/>
	<div class="row clearfix">
		<div class="col-md-2 column">
			<label class="col-sm-3 control-label">Estado*:&nbsp;</label>
		</div>
		<div class="col-md-4 column">
			{{ Form::select('idEstadoUsuario', $estados ) }}
		</div>
		<div class="col-md-6 column"></div>
	</div>
	<br/>
	<div class="row clearfix">
		<div class="col-md-2 2olumn">
			<label class="col-sm-3 control-label">Resumen:&nbsp;</label>
		</div>
		<div class="col-md-6 column">
			<div class="col-md-6 column">
				{{ Form::textarea('resumenUsuario') }}
			</div>
		</div>
		<div class="col-md-6 column"></div>
	</div>
	<br/>
	
	<div class="row clearfix">
		<div class="col-md-2 column" style="text-aling:center">
			 <button type="button" id="performAction"  class="btn btn-primary btn-default">Actualizar Informaci&oacute;n</button>
		</div>
		<div class="col-md-2 column" style="text-aling:center">
			 <button type="button" id="regresar"  class="btn btn-primary btn-default">Regresar</button>
		</div>

	</div>
</div>
{{ Form::close() }}


<script type="text/javascript">

	$(document).ready(function()
	{
		var checkMessagesServer = $("[name='errmessage']").val();
		if(checkMessagesServer && checkMessagesServer  != '')
		{	alertify.error(checkMessagesServer); $("[name='emailUsuario']").parent().removeClass('has-error').addClass('has-error');}
		
		$("[name='nombreUsuario']").addClass('form-control');
		$("[name='apelUsuario']").addClass('form-control');
		$("[name='passwordUsuario']").addClass('form-control');
		$("[name='emailUsuario']").addClass('form-control');
		$("[name='idEstadoUsuario']").addClass('choosen');
		$("[name='idEstadoUsuario']").chosen({disable_search_threshold: 10});
		$("[name='resumenUsuario']").addClass('form-control');
		
		$("#regresar").click(function()
		{
			window.location.replace("{{ URL::action('AdminusuariosController@index')}}");
		});
		
		$("#performAction").click(function()
		{
			$("[name='nombreUsuario']").parent().removeClass('has-error');
			$("[name='apelUsuario']").parent().removeClass('has-error');
			$("[name='passwordUsuario']").parent().removeClass('has-error');
			$("[name='confirmPassword']").parent().removeClass('has-error');
			$("[name='emailUsuario']").parent().removeClass('has-error');
			
			var errors = false;
			var password = $("[name='passwordUsuario']").val();
			var passwordConfirm = $("[name='confirmPassword']").val();
			
			if(password.length < 7 && !$("[name='nocambiar']").is(":checked"))
			{
				alertify.error("La contrase&ntilde;a debe de tener mas de 6 caracteres!");
				$("[name='passwordUsuario']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			
			if(password != passwordConfirm && !$("[name='nocambiar']").is(":checked"))
			{
				alertify.error("Passwords no coinciden");
				$("[name='passwordUsuario']").parent().removeClass('has-error').addClass('has-error');
				$("[name='confirmPassword']").parent().removeClass('has-error').addClass('has-error');
				$("[name='passwordUsuario']").val('');
				$("[name='confirmPassword']").val('');
				errors = true;
			}
			
			var correo = $("[name='emailUsuario']").val();
			var correoRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			if(!correoRegex.test(correo))
			{
				alertify.error("Correo invalido!");
				$("[name='emailUsuario']").parent().removeClass('has-error').addClass('has-error');
				errors = true;
			}
			
			if(!errors)
			{
				$('#formEdicion').submit();
			}
		});
	});
</script>

@stop