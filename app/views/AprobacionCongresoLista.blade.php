@section('content')
<style>
   .table td{
    cursor:pointer;
   }
 </style>
<div class="container">
		<div class="row">
			<div class="col-md-12 column">
				<h3 class="text-center">
					Lista de Congresos por Aprobar
				</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 column">

				<table id="tblCongresos" class="display">
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Nombre del congreso
							</th>
							<th>
								Acr&oacute;nimo
							</th>
							<th>
								Usuario Solicitante
							</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($congresos as $congreso)
							<tr class="clickRow" style="cursor:pointer;">
								<td visible="false">
									{{$congreso->idCongreso}}
								</td>
								<td>
									{{$congreso->nomCongreso}}
								</td>
								<td>
									{{$congreso->acronimoCongreso}}
								</td>
								<td>
									{{Usuario::Find($congreso->idCreador)->nombreUsuario . ' ' . Usuario::Find($congreso->idCreador)->apelUsuario}}
								</td>
							</tr>
								
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
</div>

<script type="text/javascript">

	$(document).ready(function()
	{
		$('#tblCongresos').DataTable({
            "language": {
				"sProcessing":     "Procesando...",
				"sLengthMenu":     "Mostrar _MENU_ registros",
				"sZeroRecords":    "No se encontraron resultados",
				"sEmptyTable":     "Ningún dato disponible en esta tabla",
				"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				"sInfoPostFix":    "",
				"sSearch":         "Buscar:",
				"sUrl":            "",
				"sInfoThousands":  ",",
				"sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
				"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
            }
		});

	   	$("#tblCongresos tbody" ).on('click', 'tr.clickRow',  function()
		{
			var id= $(this).children().eq(0).html();
            window.location.replace("AprobacionCongresoDetalles/" + id);  
		});
	});
</script>

@stop