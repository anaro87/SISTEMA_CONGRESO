@section('content')

<div class="col-sm-12" style="margin-top: 50px;">
	<div class="panel panel-primary" style="border-color: #205081; border-radius: 2px; box-shadow: 0 5px 10px gray;">
		<div class="panel-heading" style="border-radius: 0px; background: #205081; border-color: #205081;">
			<div class="panel-title">Configurar Matriz de evaluaci&oacute;n</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-12">
					<form role="form" class="form-horizontal">
						<legend style="padding-top: 10px">Matriz de evaluaci&oacute;n para fichas:</legend>
						<div class="form-group">
							<div class="col-sm-12">
								<div style="width: 100%; margin-bottom: 15px; overflow-x: scroll; overflow-y: hidden; max-width: 100%">
									<table class="table table-bordered" style="white-space: nowrap; text-align: center;" id="criteriosFicha">
										<thead>
											<tr>
												<th>Eliminar</th>
												<th>Nombre del criterio:</th>
												<th>Agrupar:</th>
												<th colspan="1000">Puntaje:</th>
											</tr>
										</thead>
										@foreach ($criteriosFicha as $criterioFicha)
										@if ($criterioFicha->criterioGrupal)
										<tbody data-id="{{$criterioFicha->idCriterioEvaluacion}}">
											<tr>
												<td rowspan="4" style="vertical-align: middle;">
													<button type="button" class="btn btn-danger eliminarCriterio">
														<span class="glyphicon glyphicon-minus"></span>
													</button>
												</td>
												<td rowspan="4" style="vertical-align: middle;">
													<input type="text" value="{{$criterioFicha->nombreCriterio}}"/>
												</td>
												<td rowspan="4" style="vertical-align: middle;">
													<input type="checkbox" checked="checked" class="grupal"/>
												</td>
												<td></td>
												@for ($i = 1; $i <= $criterioFicha->cantidadHijos; $i++)
												<th>Criterio #{{$i}}:</th>
												@endfor
												<th colspan="1000" class="headerHijos">Controles:</th>
											</tr>
											<tr>
												<th>Criterios dentro del grupo:</th>
												@foreach ($criterioFicha->hijos as $hijo)
												<td><input data-id="{{$hijo->idCriterioEvaluacion}}" type="text" value="{{$hijo->nombreCriterio}}"/></td>
												@endforeach
												<td colspan="1000" class="bodyHijos">
													<div class="btn-group">
														<button type="button" class="btn btn-primary btn-xs agregarCriterioHijo">
															<span class="glyphicon glyphicon-plus"></span>
														</button>
														<button type="button" class="btn btn-danger btn-xs eliminarCriterioHijo">
															<span class="glyphicon glyphicon-minus"></span>
														</button>															
													</div>
												</td>
											</tr>
											<tr>
												<td></td>
												@for ($i = 1; $i <= $criterioFicha->puntajesMax; $i++)
												<th>Equivale a {{$i - 1}}:</th>
												@endfor
												<th colspan="1000" class="headerPuntaje">Controles:</th>
											</tr>
											<tr>
												<th>Descripcion:</th>
												@foreach ($criterioFicha->puntajes as $puntaje)
												<td><input data-id="{{$puntaje->idPuntaje}}" data-valor="{{$puntaje->valorPuntaje}}" type="text" value="{{$puntaje->nomPuntaje}}" /></td>
												@endforeach
												<td colspan="1000" class="bodyPuntaje">
													<div class="btn-group">
														<button type="button" class="btn btn-primary btn-xs agregarPuntaje">
															<span class="glyphicon glyphicon-plus"></span>
														</button>
														<button type="button" class="btn btn-danger btn-xs eliminarPuntaje">
															<span class="glyphicon glyphicon-minus"></span>
														</button>															
													</div>
												</td>
											</tr>
										</tbody>
										@else
										<tbody data-id="{{$criterioFicha->idCriterioEvaluacion}}">
											<tr>
												<td rowspan="2" style="vertical-align: middle;">
													<button type="button" class="btn btn-danger eliminarCriterio">
														<span class="glyphicon glyphicon-minus"></span>
													</button>														
												</td>
												<td rowspan="2" style="vertical-align: middle;">
													<input type="text" value="{{$criterioFicha->nombreCriterio}}"/>
												</td>
												<td rowspan="2" style="vertical-align: middle;">
													<input type="checkbox" class="grupal"/>
												</td>
												<td></td>
												@for ($i = 1; $i <= $criterioFicha->puntajesMax; $i++)
												<th>Equivale a {{$i - 1}}:</th>
												@endfor
												<th colspan="1000" class="headerPuntaje">Controles:</th>
											</tr>
											<tr>
												<th>Descripcion:</th>
												@foreach ($criterioFicha->puntajes as $puntaje)
												<td><input data-id="{{$puntaje->idPuntaje}}" data-valor="{{$puntaje->valorPuntaje}}" type="text" value="{{$puntaje->nomPuntaje}}"/></td>
												@endforeach
												<td colspan="1000" class="bodyPuntaje">
													<div class="btn-group">
														<button type="button" class="btn btn-primary btn-xs agregarPuntaje">
															<span class="glyphicon glyphicon-plus"></span>
														</button>
														<button type="button" class="btn btn-danger btn-xs eliminarPuntaje">
															<span class="glyphicon glyphicon-minus"></span>
														</button>															
													</div>
												</td>
											</tr>
										</tbody>
										@endif
										@endforeach
										<tfoot>
											<tr>
												<td colspan="1000" class="depMaxPuntajes">
													<div class="container-fluid">
														<div class="row">
															<div class="col-sm-12">
																<div class="btn-group">
																	<button type="button" class="btn btn-primary" id="guardarCriteriosFicha">
																		<span class="glyphicon glyphicon-floppy-save"></span> <strong>Guardar matriz</strong>
																	</button>
																	<button type="button" class="btn btn-success" id="agregarCriterioFicha">
																		<span class="glyphicon glyphicon-plus"></span> <strong>Agregar un nuevo criterio</strong>
																	</button>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										</tfoot>
									</table>							
								</div>
							</div>
						</div>
						<legend style="padding-top: 10px">Matriz de evaluaci&oacute;n para art&iacute;culos:</legend>
						<div class="form-group">
							<div class="col-sm-12">
								<div style="width: 100%; margin-bottom: 15px; overflow-x: scroll; overflow-y: hidden; max-width: 100%;" class="containerTable">
									<table class="table table-bordered" style="white-space: nowrap; text-align: center;" id="criteriosPapers">
										<thead>
											<tr>
												<th>Eliminar</th>
												<th>Nombre del criterio:</th>
												<th>Agrupar:</th>
												<th colspan="1000" class="depMaxPuntajes">Puntaje:</th>
											</tr>
										</thead>
										@foreach ($criteriosPaper as $criterioPaper)
										@if ($criterioPaper->criterioGrupal)
										<tbody data-id="{{$criterioPaper->idCriterioEvaluacion}}">
											<tr>
												<td rowspan="4" style="vertical-align: middle;">
													<button type="button" class="btn btn-danger eliminarCriterio">
														<span class="glyphicon glyphicon-minus"></span>
													</button>														
												</td>
												<td rowspan="4" style="vertical-align: middle;">
													<input type="text" value="{{$criterioPaper->nombreCriterio}}"/>
												</td>
												<td rowspan="4" style="vertical-align: middle;">
													<input type="checkbox" checked="checked" class="grupal"/>
												</td>
												<td></td>
												@for ($i = 1; $i <= $criterioPaper->cantidadHijos; $i++)
												<th>Criterio #{{$i}}:</th>
												@endfor
												<th colspan="1000" class="headerHijos">Controles:</th>
											</tr>
											<tr>
												<th>Criterios dentro del grupo:</th>
												@foreach ($criterioPaper->hijos as $hijo)
												<td><input data-id="{{$hijo->idCriterioEvaluacion}}" type="text" value="{{$hijo->nombreCriterio}}"/></td>
												@endforeach
												<td colspan="1000" class="bodyHijos">
													<div class="btn-group">
														<button type="button" class="btn btn-primary btn-xs agregarCriterioHijo">
															<span class="glyphicon glyphicon-plus"></span>
														</button>
														<button type="button" class="btn btn-danger btn-xs eliminarCriterioHijo">
															<span class="glyphicon glyphicon-minus"></span>
														</button>
													</div>
												</td>
											</tr>
											<tr>
												<td></td>
												@for ($i = 1; $i <= $criterioPaper->puntajesMax; $i++)
												<th>Equivale a {{$i -1}}:</th>
												@endfor
												<th colspan="1000" class="headerPuntaje">Controles:</th>
											</tr>
											<tr>
												<th>Descripcion:</th>
												@foreach ($criterioPaper->puntajes as $puntaje)
												<td><input data-id="{{$puntaje->idPuntaje}}" data-valor="{{$puntaje->valorPuntaje}}" type="text" value="{{$puntaje->nomPuntaje}}" /></td>
												@endforeach
												<td colspan="1000" class="bodyPuntaje">
													<div class="btn-group">
														<button type="button" class="btn btn-primary btn-xs agregarPuntaje">
															<span class="glyphicon glyphicon-plus"></span>
														</button>
														<button type="button" class="btn btn-danger btn-xs eliminarPuntaje">
															<span class="glyphicon glyphicon-minus"></span>
														</button>
													</div>
												</td>
											</tr>
										</tbody>
										@else
										<tbody data-id="{{$criterioPaper->idCriterioEvaluacion}}">
											<tr>
												<td rowspan="2" style="vertical-align: middle;">
													<button type="button" class="btn btn-danger eliminarCriterio">
														<span class="glyphicon glyphicon-minus"></span>
													</button>
												</td>
												<td rowspan="2" style="vertical-align: middle;">
													<input type="text" value="{{$criterioPaper->nombreCriterio}}"/>
												</td>
												<td rowspan="2" style="vertical-align: middle;">
													<input type="checkbox" class="grupal"/>
												</td>
												<td></td>
												@for ($i = 1; $i <= $criterioPaper->puntajesMax; $i++)
												<th>Equivale a {{$i -1}}:</th>
												@endfor
												<th colspan="1000" class="headerPuntaje">Controles:</th>
											</tr>
											<tr>
												<th>Descripcion:</th>
												@foreach ($criterioPaper->puntajes as $puntaje)
												<td><input data-id="{{$puntaje->idPuntaje}}" data-valor="{{$puntaje->valorPuntaje}}" type="text" value="{{$puntaje->nomPuntaje}}"/></td>
												@endforeach
												<td colspan="1000" class="bodyPuntaje">
													<div class="btn-group">
														<button type="button" class="btn btn-primary btn-xs agregarPuntaje">
															<span class="glyphicon glyphicon-plus"></span>
														</button>
														<button type="button" class="btn btn-danger btn-xs eliminarPuntaje">
															<span class="glyphicon glyphicon-minus"></span>
														</button>															
													</div>
												</td>
											</tr>
										</tbody>
										@endif
										@endforeach
										<tfoot>
											<tr>
												<td colspan="1000" class="depMaxPuntajes">
													<div class="container-fluid">
														<div class="row">
															<div class="col-sm-12">
																<div class="btn-group">
																	<button type="button" class="btn btn-primary" id="guardarCriteriosPaper">
																		<span class="glyphicon glyphicon-floppy-save"></span> <strong>Guardar matriz</strong>
																	</button>
																	<button type="button" class="btn btn-success" id="agregarCriterioPaper">
																		<span class="glyphicon glyphicon-plus"></span> <strong>Agregar un nuevo criterio</strong>
																	</button>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										</tfoot>
									</table>							
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-4">
					<button type="button" id="cancelarTodo" class="btn btn-default btn-default">Regresar</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="x-tmpl-mustache" id="templateIndividual">
	<tbody>
		<tr>
			<td rowspan="2" style="vertical-align: middle;">
				<button type="button" class="btn btn-danger eliminarCriterio">
					<span class="glyphicon glyphicon-minus"></span>
				</button>														
			</td>
			<td rowspan="2" style="vertical-align: middle;">
				<input type="text" value=""/>
			</td>
			<td rowspan="2" style="vertical-align: middle;">
				<input type="checkbox" class="grupal"/>
			</td>
			<td></td>
			<th colspan="1000" class="headerPuntaje">Controles:</th>
		</tr>
		<tr>
			<th>Descripcion:</th>
			<td colspan="1000" class="bodyPuntaje">
				<div class="btn-group">
					<button type="button" class="btn btn-primary btn-xs agregarPuntaje">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
					<button type="button" class="btn btn-danger btn-xs eliminarPuntaje">
						<span class="glyphicon glyphicon-minus"></span>
					</button>															
				</div>
			</td>
		</tr>
	</tbody>
</script>
<script type="x-tmpl-mustache" id="templateGrupal">
	<tbody>
		<tr>
			<td rowspan="4" style="vertical-align: middle;">
				<button type="button" class="btn btn-danger eliminarCriterio">
					<span class="glyphicon glyphicon-minus"></span>
				</button>
			</td>
			<td rowspan="4" style="vertical-align: middle;">
				<input type="text" value=""/>
			</td>
			<td rowspan="4" style="vertical-align: middle;">
				<input type="checkbox" checked="checked" class="grupal"/>
			</td>
			<td></td>
			<th colspan="1000" class="headerHijos">Controles:</th>
		</tr>
		<tr>
			<th>Criterios dentro del grupo:</th>
			<td colspan="1000" class="bodyHijos">
				<div class="btn-group">
					<button type="button" class="btn btn-primary btn-xs agregarCriterioHijo">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
					<button type="button" class="btn btn-danger btn-xs eliminarCriterioHijo">
						<span class="glyphicon glyphicon-minus"></span>
					</button>															
				</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<th colspan="1000" class="headerPuntaje">Controles:</th>
		</tr>
		<tr>
			<th>Descripcion:</th>
			<td colspan="1000" class="bodyPuntaje">
				<div class="btn-group">
					<button type="button" class="btn btn-primary btn-xs agregarPuntaje">
						<span class="glyphicon glyphicon-plus"></span>
					</button>
					<button type="button" class="btn btn-danger btn-xs eliminarPuntaje">
						<span class="glyphicon glyphicon-minus"></span>
					</button>															
				</div>
			</td>
		</tr>
	</tbody>
</script>
<script type="text/javascript">
	var matriz = [];

	$(document).ready(function()
	{
		$('.containerTable').css('max-width',$('.containerTable').css('width'));
		eliminarCriterioEvent();
		agregarPuntajeEvent();
		eliminarPuntajeEvent();
		agregarCriterioHijoEvent();
		eliminarCriterioHijoEvent();
		grupalEvent();
		
		$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}";
		});	

		$('#agregarCriterioFicha').click(function()
		{
			agregarCriterioEvent('#criteriosFicha');
		});

		$('#agregarCriterioPaper').click(function()
		{
			agregarCriterioEvent('#criteriosPapers');
		});

		$('#guardarCriteriosFicha').click(function()
		{
			var criterios = [];
			$('#criteriosFicha tbody').each(function(index)
			{
				var puntajes = [];
				var nombreCriterioPadre = $(this).find('tr:first-child td:nth-child(2) input').val();
				var idCriterioPadre = $(this).attr('data-id');
				var grupalCriterio = $(this).find(":checked").length;
				
				$(this).find('.bodyPuntaje').siblings('td').find('input').each(function()
				{
					puntajes.push({idPuntaje : $(this).attr('data-id'), valorPuntaje : $(this).attr('data-valor'), nomPuntaje : $(this).val()});
				});
				criterios.push(
				{
					idCriterioEvaluacion 		: idCriterioPadre, 
					nombreCriterio 				: nombreCriterioPadre,
					criterioGrupal 				: grupalCriterio,
					idCriterioEvaluacionPadre 	: idCriterioPadre,
					puntajes 					: puntajes,
					indicadorFicha				: 'F',
					idTemp						: index
				});
				$(this).find('.bodyHijos').siblings('td').find('input').each(function()
				{
					criterios.push(
					{
						idCriterioEvaluacion 		: $(this).attr('data-id'), 
						nombreCriterio 				: $(this).val(),
						criterioGrupal 				: grupalCriterio,
						idCriterioEvaluacionPadre 	: idCriterioPadre,
						puntajes 					: puntajes,
						indicadorFicha				: 'F',
						idTempPadre					: index
					});
				});
			});
			//TODO: validaciones de longitud de campo, validaciones de presencias

			$.post(
				"{{URL::action('ConfigurarEvaluacionController@guardarConfigurarEval', array($idCongreso))}}",
				{criterios: criterios}
				)
			.done(function(data)
			{
				if(data.error) alertify.error(data.mensaje);
				else alertify.success(data.mensaje);
			})
			.fail(function(data, status, jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error de comunicación con el servidor.");
			});
		});

		$('#guardarCriteriosPaper').click(function()
		{
			var criterios = [];
			$('#criteriosPapers tbody').each(function(index)
			{
				var puntajes = [];
				var nombreCriterioPadre = $(this).find('tr:first-child td:nth-child(2) input').val();
				var idCriterioPadre = $(this).attr('data-id');
				var grupalCriterio = $(this).find(":checked").length;

				$(this).find('.bodyPuntaje').siblings('td').find('input').each(function()
				{
					puntajes.push({idPuntaje : $(this).attr('data-id'), valorPuntaje : $(this).attr('data-valor'), nomPuntaje : $(this).val()});
				});
				criterios.push(
				{
					idCriterioEvaluacion 		: idCriterioPadre, 
					nombreCriterio 				: nombreCriterioPadre,
					criterioGrupal 				: grupalCriterio,
					idCriterioEvaluacionPadre 	: idCriterioPadre,
					puntajes 					: puntajes,
					indicadorFicha				: 'P',
					idTemp						: index
				});
				$(this).find('.bodyHijos').siblings('td').find('input').each(function()
				{
					criterios.push(
					{
						idCriterioEvaluacion 		: $(this).attr('data-id'), 
						nombreCriterio 				: $(this).val(),
						criterioGrupal 				: grupalCriterio,
						idCriterioEvaluacionPadre 	: idCriterioPadre,
						puntajes 					: puntajes,
						indicadorFicha				: 'P',
						idTempPadre					: index
					});
				});
			});
					//TODO: validaciones de longitud de campo, validaciones de presencias

			$.post(
				"{{URL::action('ConfigurarEvaluacionController@guardarConfigurarEval', array($idCongreso))}}",
				{criterios: criterios}
			)
			.done(function(data)
			{
				if(data.error) alertify.error(data.mensaje);
				else alertify.success(data.mensaje);
			})
			.fail(function(data, status, jqXHR)
			{
				console.log("Server Returned " + status);
				alertify.error("Error de comunicación con el servidor.");
			});
		});
	});

function agregarCriterioEvent(selector)
{
	$(selector +' tfoot').before(Mustache.render($("#templateIndividual").html(), null));
	eliminarCriterioEvent(selector + ' tbody:last');
	agregarPuntajeEvent(selector + ' tbody:last');
	eliminarPuntajeEvent(selector + ' tbody:last');
	grupalEvent(selector + ' tbody:last');

	var $headerPuntaje = $(selector + ' tbody:last .agregarPuntaje').parents('tbody').find('.headerPuntaje');
	var $bodyPuntaje = $(selector + ' tbody:last .agregarPuntaje').parents('tbody').find('.bodyPuntaje');
	$headerPuntaje.before("<th>Equivale a 0:</th>");
	$headerPuntaje.before("<th>Equivale a 1:</th>");
	$bodyPuntaje.before("<td><input data-valor='0' type='text' /></td>");
	$bodyPuntaje.before("<td><input data-valor='1' type='text' /></td>");
	$(selector + ' tbody:last .agregarPuntaje').parents('table').parent().animate({ scrollLeft: 10000}, 100);
}

function eliminarCriterioEvent(selector)
{
	selector = (typeof(selector) === 'undefined') ? '.eliminarCriterio' : (typeof(selector) === 'string') ? selector + ' .eliminarCriterio' : $(selector).find('.eliminarCriterio');
	$(selector).click(function()
	{
		$(this).parents('tbody').remove();
	});
}

function agregarPuntajeEvent(selector)
{
	selector = (typeof(selector) === 'undefined') ? '.agregarPuntaje' : (typeof(selector) === 'string') ? selector + ' .agregarPuntaje' : $(selector).find('.agregarPuntaje');
	$(selector).click(function()
	{
		var $headerPuntaje = $(this).parents('tbody').find('.headerPuntaje');
		var $bodyPuntaje = $(this).parents('tbody').find('.bodyPuntaje');
		var puntajesActual = $headerPuntaje.siblings('th').size();
		$headerPuntaje.before("<th>Equivale a " + (parseInt(puntajesActual))  + ":</th>");
		$bodyPuntaje.before("<td><input data-valor='" + (parseInt(puntajesActual)) + "' type='text' /></td>");
		$(this).parents('table').parent().animate({ scrollLeft: 10000}, 100);
	});	
}

function eliminarPuntajeEvent(selector)
{
	selector = (typeof(selector) === 'undefined') ? '.eliminarPuntaje' : (typeof(selector) === 'string') ? selector + ' .eliminarPuntaje' : $(selector).find('.eliminarPuntaje');
	$(selector).click(function()
	{
		var $headerPuntaje = $(this).parents('tbody').find('.headerPuntaje');
		var $bodyPuntaje = $(this).parents('tbody').find('.bodyPuntaje');
		var puntajesActual = $headerPuntaje.siblings('th').size();
		if(puntajesActual > 2)
		{
			$headerPuntaje.prev().not('td').remove();
			$bodyPuntaje.prev().not('th').remove();
		}
	});
}

function agregarCriterioHijoEvent(selector)
{
	selector = (typeof(selector) === 'undefined') ? '.agregarCriterioHijo' : (typeof(selector) === 'string') ? selector + ' .agregarCriterioHijo' : $(selector).find('.agregarCriterioHijo');
	$(selector).click(function()
	{
		var $headerPuntaje = $(this).parents('tbody').find('.headerHijos');
		var $bodyPuntaje = $(this).parents('tbody').find('.bodyHijos');
		var puntajesActual = $headerPuntaje.siblings('th').size();
		$headerPuntaje.before("<th>Criterio #" + (parseInt(puntajesActual) + 1)  + ":</th>");
		$bodyPuntaje.before("<td><input type='text' /></td>");
		$(this).parents('table').parent().animate({ scrollLeft: 10000}, 100);
	});		
}

function eliminarCriterioHijoEvent(selector)
{
	selector = (typeof(selector) === 'undefined') ? '.eliminarCriterioHijo' : (typeof(selector) === 'string') ? selector + ' .eliminarCriterioHijo' : $(selector).find('.eliminarCriterioHijo');
	$(selector).click(function()
	{
		var $headerPuntaje = $(this).parents('tbody').find('.headerHijos');
		var $bodyPuntaje = $(this).parents('tbody').find('.bodyHijos');
		var puntajesActual = $headerPuntaje.siblings('th').size();
		if(puntajesActual > 2)
		{
			$headerPuntaje.prev().not('td').remove();
			$bodyPuntaje.prev().not('th').remove();
		}
	});
}

function grupalEvent(selector)
{
	selector = (typeof(selector) === 'undefined') ? '.grupal' : (typeof(selector) === 'string') ? selector + ' .grupal' : $(selector).find('.grupal');
	$(selector).click(function()
	{
		var template = $(this).filter(":checked").length ? "#templateGrupal" : "#templateIndividual";
		var $criterioAnterior = $(this).parents('tbody');
		$criterioAnterior.after(Mustache.render($(template).html(), null));
		var $criterioNuevo = $criterioAnterior.next('tbody');

		$criterioNuevo.find('tr:first-child td:nth-child(2) input').val($criterioAnterior.find('tr:first-child td:nth-child(2) input').val());
		$criterioNuevo.find('.headerPuntaje').before($criterioAnterior.find('.headerPuntaje').siblings('th'));
		$criterioNuevo.find('.bodyPuntaje').before($criterioAnterior.find('.bodyPuntaje').siblings('td'));
		$criterioNuevo.find('.headerHijos').before("<th>Criterio #1:</th>");
		$criterioNuevo.find('.bodyHijos').before("<td><input type='text' /></td>");

		eliminarCriterioEvent($criterioNuevo);
		agregarPuntajeEvent($criterioNuevo);
		eliminarPuntajeEvent($criterioNuevo);
		agregarCriterioHijoEvent($criterioNuevo);
		eliminarCriterioHijoEvent($criterioNuevo);
		grupalEvent($criterioNuevo);

		$criterioAnterior.remove();
	});
}
</script>
@stop