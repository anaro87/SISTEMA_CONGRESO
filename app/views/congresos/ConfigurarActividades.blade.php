@section('content')
	<div class="container">
	@if( $congresoOwner > 0 )
		<div class="row clearfix">
			<div class="col-md-12 column">
				<h3 class="text-center">
					Actividades para congreso "{{ $nombreCongreso }}"
				</h3>
				<div class="alert alert-info fade in" style="margin-right: 30px;" >
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					<strong>Importante</strong>
					<br/><br/>
					<strong>1) </strong> Puede definir tantas actividades como usted necesite para un congreso.
					<br/><br/>
					<strong>2) </strong> Ninguna actividad está asociada a una temática o sesión, por lo que no está limitada a la hora de seleccionar el horario en que se desea asignar.
					<br/><br/>
					<strong>3) </strong> Se puede crear una actividad y asignarla tantas veces como se desee, según las necesidades del congreso.
				</div>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-md-6 column">
				<a id="agregarActividad"  href="">Agregar Actividad</a>
				<br/><br/>
				<table id="tbl_actividades" name="tbl_actividades" class="table display">
					<thead>
						<tr>
							<th>Actividad</th>
							<th>Responsable</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@foreach ($actividadesCongreso as $objeto)
						<tr class="clickRow">
							<td id="actividadNombre_{{$objeto->idActividad}}" visible="false">
								{{$objeto->titulo}}
							</td>
							<td id="actividadResponsable_{{$objeto->idActividad}}" visible="false">
								{{$objeto->responsableAct}}
							</td>
							<td><a id="{{$objeto->idActividad}}" class="modificar" href='congresoActividades' title="Modificar">Modificar</a></td>
							<td><a id="{{$objeto->idActividad}}" class="eliminar"  href='congresoActividades' title="Eliminar">Eliminar</a></td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
			
			<br/>
			
			<div class="col-md-5 column" style="box-shadow: 0 5px 8px gray;">
			
				<h3 id="ACME_LABEL" name="ACME_LABEL"></h3>
				<input type='hidden' name='action' id='action' value=""></input>
				<input type='hidden' name='idRecord' id='idRecord' value=""></input>
				<br/>
				
				<div class="row clearfix">
					<div class="col-md-4 column">
						<label class="col-sm-3 control-label">Actividad: &nbsp;</label>
					</div>
					<div class="col-md-6 column">
						<input name="nomActividad" id="nomActividad" class="form-control" disabled='' type="text" ></input>
					</div>
					<div class="col-md-4 column"></div>
				</div>
				<br/>

				<div class="row clearfix">
					<div class="col-md-4 column">
						<label class="col-sm-3 control-label">Responsable: &nbsp;</label>
					</div>
					<div class="col-md-6 column">
						<input name="responsable" id="responsable" class="form-control" disabled='' type="text" ></input>
					</div>
					<div class="col-md-4 column"></div>
				</div>
				<br/>
				<!-- actividadAulaSelect -->
				<div class="row clearfix">
					<div class="col-md-6 column" style="text-aling:center">
						 <button type="button" id="performAction" name="performAction"  disabled=''  class="btn btn-primary btn-default">Guardar</button>
					</div>
				</div>
				<br/>
			</div>
			
		</div>
	@else
		<div class="row clearfix">
			<div class="col-md-12 column">
				<h3 class="text-center text-error">
					No tiene permisos para editar informaci&oacute;n sobre este congreso.
				</h3>
			</div>
		</div>
	@endif

</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#tbl_actividades').dataTable( {
	        "language": {
	        	"lengthMenu"	: "Mostrar _MENU_ por p&aacute;gina",
	            "zeroRecords"	: "No hay coincidencias",
	            "info"			: "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
	            "infoEmpty"		: "No hay records disponibles",
	            "infoFiltered"	: "(filtrado de un total de _MAX_ registros)",
	            "sSearch"		: "Buscar:",
	            "sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "&Uacute;ltimo",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
	        }
    	} );

		$("#tbl_actividades tbody" ).on('click', 'a.eliminar',  function()
		{
			var gotToURL = this.getAttribute('href');
			var recId	 = this.id;
			alertify.confirm("¿Está seguro quiere borrar este record?", function (e) {
				if (e) {
					$.post("{{ URL::action('CongresoActividadesController@actualizarData') }}", {action:'DELETE', currentId : recId,idDC : {{$idCongreso}} })
						.done(function(data, status,jqXHR)
						{
							if(data.error)
							{
								alertify.error(data.mensaje);
							}
							else
							{
								alertify.success(data.mensaje);
								window.setTimeout(function()
								{
									window.location.replace("{{URL::action('CongresoActividadesController@getIndex',array($idCongreso) )}}");
								},
								900);
								
							}
						})
						.fail(function(data, status,jqXHR)
						{
							console.log("Server Returned " + status);
							alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
						});
				}
				else {return false;}
			
			});
			return false;
		});
		
		$("#tbl_actividades tbody" ).on('click', 'a.modificar',  function()
		{
			document.getElementById('ACME_LABEL').innerHTML = 'Modificar';
			var objId = this.id;
			
			nombreAct = document.getElementById('actividadNombre_'+objId).innerHTML;
			nombreAct = nombreAct.replace(/\n|\r/g, "").trim();
			document.getElementById('nomActividad').value = nombreAct;
			document.getElementById('nomActividad').removeAttribute('disabled');
			document.getElementById('nomActividad').focus();

			responsable = document.getElementById('actividadResponsable_'+objId).innerHTML;
			responsable = responsable.replace(/\n|\r/g, "").trim();
			document.getElementById('responsable').value = responsable;
			document.getElementById('responsable').removeAttribute('disabled');

			$("[name='performAction']").removeAttr('disabled');
			document.getElementById('action').value = 'UPDATE';
			document.getElementById('idRecord').value = objId;
			
			return false;
		});
		
		$("#agregarActividad").click(function()
		{
			document.getElementById('ACME_LABEL').innerHTML = 'Agregar';
			document.getElementById('action').value = 'ADD';

			$("[name='performAction']").removeAttr('disabled');
			$("[name='nomActividad']").removeAttr('disabled');
			$("[name='nomActividad']").val('');
			document.getElementById('nomActividad').focus();

			$("[name='responsable']").removeAttr('disabled');
			$("[name='responsable']").val('');
			return false;
		});
		
		
		$("#performAction").click(function()
		{
			var pactividad	= document.getElementById('nomActividad').value.trim();
			var presponsable	= document.getElementById('responsable').value.trim();
			var paction		= document.getElementById('action').value.trim();
			var pcurrentId	= document.getElementById('idRecord').value.trim();
			if (checkFieldsEmpty('nomActividad','Ingrese un valor para actividad'))
				return;

			$.post("{{ URL::action('CongresoActividadesController@actualizarData') }}", {actividad: pactividad, responsable : presponsable, aula:'0', action:paction,currentId:pcurrentId ,idDC : {{$idCongreso}} })
					.done(function(data, status,jqXHR)
					{
						if(data.error)
						{
							alertify.error(data.mensaje);
						}
						else
						{
							alertify.success(data.mensaje);
							window.setTimeout(function()
							{
								window.location.replace("{{URL::action('CongresoActividadesController@getIndex',array($idCongreso) )}}");
							},
							900);
							
						}
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
					});
		});
		function checkFieldsEmpty(idControl,mensajeStop)
		{
			var detener = false;
			var controlHTML	= $("[name='"+idControl+"']").val().trim();
			if(controlHTML == '')
			{
				alertify.error(mensajeStop);
				$("[name='"+idControl+"']").parent().removeClass('has-error').addClass('has-error');
				detener = true;
			}else{$("[name='"+idControl+"']").parent().removeClass('has-error');}
			return detener;
		};
		
		
	});
</script>

@stop