@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="page-header" style="margin-top: 0px !important;">
			<h3>
				T&iacute;tulo: {{$paper->tituloPaper}}
			</h3>
		</div>
	</div>
	<div class="col-sm-6" style="border-right: 1px solid #eee;margin-top: -21px;padding-top: 20px;">
		<div class="row">
			<div class="col-sm-12">
				<form role="form" class="form-horizontal">
					<fieldset>
						<div class="form-group">
							<label class="col-sm-4 control-label">Autor encargado:</label>
							<div class="col-sm-8">
								<span class="form-control" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
									{{$autores[0]->autores}}
								</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Tem&aacute;tica:</label>
							<div class="col-sm-8">
								<span class="form-control" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
									{{$paper->nomTematica}}
								</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Categor&iacute;a:</label>
							<div class="col-sm-8">
								<span class="form-control" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
									{{$paper->nomCategoria}}
								</span>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Fecha de Creaci&oacute;n:</label>
							<div class="col-sm-8">
								<span class="form-control" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">{{$paper->created_at}}</span>
							</div>
						</div>
						<div class="form-group">
							<label  class="col-sm-4 control-label">Resumen:</label>
							<div class="col-sm-8">
								<textarea class="form-control" rows="5" style="max-width: 100%; cursor: default; background-color: white;" readonly="readonly">{{trim($paper->resumenPaper)}}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Palabras Clave:</label>					
							<div class="col-sm-8">
								<textarea class="form-control" rows="4" style="max-width: 100%; cursor: default; background-color: white;" readonly="readonly">{{trim($paper->palabrasClaves)}}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Fuentes:</label>
							<div class="col-sm-8">
								@if($paper->rutaFuentes == '')
								<span class="col-sm-9">No dispone de archivo fuente</span>
								@else
								<span class="btn btn-link form-control" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
									<a href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$paper->rutaFuentes)) }}"><strong>Link de descarga</strong></a>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-4 control-label">Archivo:</label>
							<div class="col-sm-8">
								<span class="btn btn-link form-control" style="text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
									<a href="{{ URL::action('ArchivoController@servirArchivo', array($idCongreso,$paper->rutaFicha)) }}"><strong>Link de descarga</strong></a>
								</span>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label class="col-sm-4 control-label">Opciones:</label>
							<div class="col-sm-8" style="text-align: center;">
								<div class="btn-group">
									<button type="button" class="aprobarFilaDB btn btn-success" data-toggle='tooltip' data-placement='left' title='Aprobar'>
										<i class="glyphicon glyphicon-ok"></i> Aprobar
									</button>
									<button type="button" class="eliminarFilaDB btn btn-danger" data-toggle='tooltip' data-placement='left' title='Rechazar'>
										<i class="glyphicon glyphicon-remove"></i> Rechazar
									</button>
									<button type="button" id="cancelarTodo"class="btn btn-default" data-toggle='tooltip' data-placement='left' title='Regresar'>
										Regresar
									</button>
								</div>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<div class="col-sm-6" style="margin-top: -21px;">
		<div style="width: 100%; margin-bottom: 15px; overflow-x: auto; overflow-y: auto; max-height: 529px;" id="revisiones">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-header" style="margin-top: 0px !important; margin-bottom: 10px">
						<h3>
							Promedio de las revisiones:
						</h3>
					</div>
				</div>
			</div>
			<table class="table table-bordered" style="text-align: left; margin-bottom: 5px; max-width: 98.6%;">
				<thead>
					<tr>
						<th>Criterio:</th>
						<th>Evaluaci&oacute;n:</th>
					</tr>
				</thead>
				@foreach ($criterios as $criterio)
				
				@if ($criterio->idCriterioEvaluacion == $criterio->idCriterioEvaluacionPadre and $criterio->criterioGrupal == 0)
				<tbody>
					<tr>
						<td style="text-align: right;">
							<span>{{$criterio->nombreCriterio}}:</span>
						</td>
						<td style="text-align: center;">
							<input type="text" data-slider-min="1" data-slider-enabled="false" data-slider-max="{{$criterio->puntajesMax}}" data-slider-step="0.01" data-slider-value="1" data-slider-labels='{{json_encode($criterio->puntajes)}}' data-criterio-id="{{$criterio->idCriterioEvaluacion}}" class="sliderEval"/>
						</td>
					</tr>
				</tbody>
				@endif
				@if ($criterio->idCriterioEvaluacion == $criterio->idCriterioEvaluacionPadre and $criterio->criterioGrupal != 0)
				<tbody style="border-top: 3px solid #ddd; border-bottom: 3px solid #ddd">
					<tr>
						<td colspan="2" style="text-align: center">
							<span><strong>Grupo: {{$criterio->nombreCriterio}}</strong></span>
						</td>
					</tr>
					@foreach ($criterio->hijos as $hijo)
					<tr>
						<td style="text-align: right;">
							<span>{{$hijo->nombreCriterio}}:</span>
						</td>
						<td style="text-align: center">
							<input type="text" data-slider-min="1" data-slider-enabled="false" data-slider-max="{{$hijo->puntajesMax}}" data-slider-step="0.01" data-slider-value="1" data-slider-labels='{{json_encode($hijo->puntajes)}}' data-criterio-id="{{$hijo->idCriterioEvaluacion}}" class="sliderEval"/>
						</td>
					</tr>
					@endforeach
					@endif
				</tbody>
				@endforeach
				<tfoot>
					<tr>
						<td colspan="2"  style="text-align: right;">
							<span>Total de puntos obtenidos: <strong>{{$suma}}/{{$max}}</strong> {{$max==0?0:round($suma/$max, 2)*100}}%</span>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
		<hr>
	</div>
</div>
<script type="x-tmpl-mustache" id="templatePaper">
<%#usuariosConRev%>
<div class="row">
	<div class="col-sm-12">
		<div class="page-header" style="margin-top: 0px !important; margin-bottom: 10px">
			<h3>
				<span>Revision de <%nombreUsuario%> <%apelUsuario%>:</span>
			</h3>
		</div>
	</div>
</div>
<table class="table table-bordered" style="text-align: left; margin-bottom: 5px;  max-width: 98.6%;">
	<thead>
		<tr>
			<th>Criterio:</th>
			<th>Evaluaci&oacute;n:</th>
		</tr>
	</thead>
	@foreach ($criterios as $criterio)
	
	@if ($criterio->idCriterioEvaluacion == $criterio->idCriterioEvaluacionPadre and $criterio->criterioGrupal == 0)
	<tbody>
		<tr>
			<td style="text-align: right;">
				<span>{{$criterio->nombreCriterio}}:</span>
			</td>
			<td style="text-align: center;">
				<input type="text" data-slider-min="1" data-slider-enabled="false" data-slider-max="{{$criterio->puntajesMax}}" data-slider-step="0.01" data-slider-value="1" data-slider-labels='{{json_encode($criterio->puntajes)}}' data-criterio-id="rev<%idRevisionFicha%>criterio{{$criterio->idCriterioEvaluacion}}" class="sliderEval"/>
			</td>
		</tr>
	</tbody>
	@endif
	@if ($criterio->idCriterioEvaluacion == $criterio->idCriterioEvaluacionPadre and $criterio->criterioGrupal != 0)
	<tbody style="border-top: 3px solid #ddd; border-bottom: 3px solid #ddd">
		<tr>
			<td colspan="2" style="text-align: center">
				<span><strong>Grupo: {{$criterio->nombreCriterio}}</strong></span>
			</td>
		</tr>
		@foreach ($criterio->hijos as $hijo)
		<tr>
			<td style="text-align: right;">
				<span>{{$hijo->nombreCriterio}}:</span>
			</td>
			<td style="text-align: center">
				<input type="text" data-slider-min="1" data-slider-enabled="false" data-slider-max="{{$hijo->puntajesMax}}" data-slider-step="0.01" data-slider-value="1" data-slider-labels='{{json_encode($hijo->puntajes)}}' data-criterio-id="rev<%idRevisionPaper%>criterio{{$hijo->idCriterioEvaluacion}}" class="sliderEval"/>
			</td>
		</tr>
		@endforeach
		@endif
	</tbody>
	@endforeach
	<tfoot>
		<tr>
			<td colspan="2"  style="text-align: right;">
				<span>Total de puntos obtenidos: <strong><%suma%>/{{$max}}</strong> <%porcentaje%>%</span>
			</td>
		</tr>
	</tfoot>
</table>
<%/usuariosConRev%>
<%^usuariosConRev%>
<div class="row">
	<div class="col-sm-4 col-sm-offset-4" style="text-align: center;">
		<h3><span class='label label-info'>No hay revisiones que mostrar</span></h3>
	</div>
</div>
<%/usuariosConRev%>
</script>
<script type="text/javascript">
	$(document).ready(function() {
		var promedios = {{json_encode($promedios)}};
		var data = {};
		data.usuariosConRev = {{json_encode($usuariosConRev)}};
		data.usuariosSinRev = {{json_encode($usuariosSinRev)}};

		$('#revisiones').append(Mustache.render($('#templatePaper').html(), data));

		$.each(data.usuariosConRev, function(index, usuario)
		{
			$.each(usuario.revisiones, function(index, revision)
			{
				$("[data-criterio-id='rev" + revision.idRevisionPaper + 'criterio' + revision.idCriterioEvaluacion + "']").attr('data-slider-value', parseFloat(revision.valorPuntaje) + 1);
			});
		});

		$.each(promedios, function (index, rev) {
			$("[data-criterio-id='" + rev.idCriterioEvaluacion + "']").attr('data-slider-value', parseFloat(rev.valorPuntaje) +1);
		});

		$(".aprobarFilaDB").click(aprobarPaper);
		$(".eliminarFilaDB").click(rechazarPaper);
		$(".sliderEval").slider({
			formater: function(valor)
			{
				var valores = eval($(this.element).attr('data-slider-labels'));
				if(parseFloat(valor) % 1 != 0)
					return valor
				return valores[valor-1].nomPuntaje;
			},
			precision: 2,
			tooltip: 'always'
		});
	});

	function aprobarPaper()
	{
		$.post("{{URL::action('UsuariosCongresosController@aprobarRechazarFP')}}",
		{
			idRegistro: "{{$paper->idPaper}}",
			tabla:"paper",
			accion:"APROBADA"
		})
		.done(function(data)
		{
			if(!data.error){
				alertify.success("Artículo aprobado exitosamente");
				window.location.href="{{URL::action('UsuariosCongresosController@listarPapers',array($idCongreso))}}";
			}
			else{
				alertify.error("Error: "+data.mensaje);
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error: "+data.mensaje);
		});
	}

	function rechazarPaper()
	{
		$.post("{{URL::action('UsuariosCongresosController@aprobarRechazarFP')}}",
		{
			idRegistro: "{{$paper->idPaper}}",
			tabla:"paper",
			accion:"RECHAZADA"

		})
		.done(function(data)
		{
			if(!data.error){
				alertify.success("El artículo ha sido rechazado.");
				window.location.href="{{URL::action('UsuariosCongresosController@listarPapers',array($idCongreso))}}";
			}
			else{
				alertify.error("Error: "+data.mensaje);
			}
		})
		.fail(function(data, status, jqXHR)
		{
			alertify.error("Error: "+data.mensaje);
		});
	}
	$("#cancelarTodo").click(function(){
			window.location.href="{{URL::action('UsuariosCongresosController@listarPapers', array($idCongreso))}}";
	});
</script>
@stop