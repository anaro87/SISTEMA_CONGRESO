@if (Auth::check())
<div class="row"  style="border-bottom: 1px solid #dddddd;">
	<div class="col-md-10 ">
		<ul class="nav nav-tabs" style="border: 0px;">
			<li ><a href="{{URL::action('HomeController@inicio')}}">
					<span class="glyphicon glyphicon-home"></span>
				</a>
			</li>
			<li class="divider-vertical"></li>
			<li ><a href="{{URL::action('UsuariosCongresosController@establecerCongreso',array($idCongreso))}}">Inicio Congreso</a></li>
			<li class="divider-vertical"></li>
			<li class="dropdown">
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Someter a Revisi&oacute;n<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::action('SumissionController@index', array($idCongreso))}}">Subir Ficha</a></li>
					<li><a href="{{URL::action('SumissionController@indexFichaLista', array($idCongreso))}}">Subir Art&iacute;culo</a></li>
					<li><a href="{{URL::action('SumissionController@listarFichas', array($idCongreso))}}">Editar Ficha</a></li>
					<li><a href="{{URL::action('SumissionController@listarArticulosEditar', array($idCongreso))}}">Editar Articulo</a></li>
					<li><a href="{{URL::action('SumissionController@listarFichasChair', array($idCongreso))}}">Modificar Ficha</a></li>
					<li><a href="{{URL::action('SumissionController@indexPresentaciones', array($idCongreso))}}">Mis Presentaciones</a></li>
					<li><a href="{{URL::action('SumissionController@indexFuentes', array($idCongreso))}}">Archivos Fuentes</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#"> Configuraci&oacute;n<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::action('UsuariosCongresosController@editarCongreso', array($idCongreso))}}" >Configuraci&oacute;n General</a></li>
					<li><a href="{{URL::action('UsuariosCongresosController@editarPostCongreso', array($idCongreso))}}">Pre-Envio y Revisiones</a></li>
					<li><a href="{{URL::action('ConfigurarEvaluacionController@configurarEvaluacion', array($idCongreso))}}">Matriz de Evaluaci&oacute;n</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Aprobaciones<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::action('UsuariosCongresosController@editarRevisoresPorPC', array($idCongreso))}}" >Gestionar Revisores</a></li>
					<li><a href="{{URL::action('UsuariosCongresosController@listarFichas', array($idCongreso))}}" >Gestionar Fichas</a></li>
					<li><a href="{{URL::action('UsuariosCongresosController@listarPapers', array($idCongreso))}}" >Gestionar Art&iacute;culos</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Congreso<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{URL::action('CongresoActividadesController@getIndex', array($idCongreso))}}">Actividades</a></li>
					<li><a href="{{URL::action('CongresoAulasController@getIndex', array($idCongreso))}}">Aulas</a></li>
					<li><a href="{{URL::action('CongresoSesionesController@getIndex', array($idCongreso))}}">Sesiones</a></li>
					<li><a href="{{ URL::asset('congreso') }}/{{$idCongreso}}/diplomas">Dise&ntilde;o Diplomas</a></li>
					<li><a href="{{ URL::asset('congreso') }}/{{$idCongreso}}/agenda">Agenda</a></li>
				</ul>
			</li>
			<li class="divider-vertical"></li>
			<li >
				<a class="dropdown-toggle" data-toggle="dropdown" href="#">Mensajes<span class="caret"></span></a>
				<ul class="dropdown-menu" role="menu">
					<li><a href="{{ URL::action('CorreosController@index', array($idCongreso)) }}">Enviar Correo</a></li>
					<li><a href="{{ URL::action('CorreosController@mostrarMail', array($idCongreso)) }}">Ver Historial</a></li>
				</ul>
			</li>
		</ul>
	</div>
	@if ($mostrar_buscar)
	<div class="col-md-4">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Buscar...">
			<span class="input-group-btn">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" ></span></button>
			</span>	
		</div>
	</div>
	@endif
</div>

@endif
