@section('content')

	 <?php
	 $tituloPantalla = '&Aacute;reas de Investigaci&oacute;n';
	 $resourceLink = 'adminareasinv';
	 $agregarLinkLabel = 'Agregar &Aacute;rea';
	 $leftDivACME = '&Aacute;rea Investigaci&oacute;n: &nbsp'; 
	 ?>
	 
	<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<h3 class="text-center">
				{{$tituloPantalla}}
			</h3>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-6 column">
			<a id="agregarCat"  href="{{URL::action('AreasInvestigacionController@create')}}">{{$agregarLinkLabel}}</a>
			<br/><br/>
			<table id="tbl_categorias" name="tbl_categorias" class="table display">
				<thead>
					<tr>
						<th>
							&Aacute;rea Investigaci&oacute;n
						</th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($areas as $area)
						<tr class="clickRow">
							<td id="catName_{{$area->idAreaInvestigacion}}" visible="false">
								{{$area->nomAreaInvestigacion}}
							</td>
							<td><a id="{{$area->idAreaInvestigacion}}" class="modificar" title="Modificar" href="{{URL::action('AreasInvestigacionController@edit',array($area->idAreaInvestigacion) )}}">Modificar</a></td>
							<td><a id="{{$area->idAreaInvestigacion}}" class="eliminar" title="Eliminar" href="{{URL::action('AreasInvestigacionController@edit',array($area->idAreaInvestigacion) )}}">Eliminar</a></td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		
		<br/>
		
		<div class="col-md-5 column" style="box-shadow: 0 5px 8px gray;"> <!-- class era de 6 -->
		
		<h3 id="lblCategoria" name="lblCategoria"></h3>
		<input type='hidden' name='gotourl' id='gotourl' value=""></input>
		<br/>
		
		<div class="row clearfix">
			<div class="col-md-4 column">
				<label class="col-sm-3 control-label">{{$leftDivACME}}</label>
			</div>
			<div class="col-md-6 column">
				<input name="nomCategoria" id="nomCategoria" class="form-control" disabled='' type="text" ></input>
			</div>
			<div class="col-md-4 column"></div>
		</div>
		<br/>
	
		<div class="row clearfix">
			<div class="col-md-6 column" style="text-aling:center">
				 <button type="button" id="performAction"  disabled=''  class="btn btn-primary btn-default">Guardar</button>
			</div>
		</div>
		<br/>
		</div>
		
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('#tbl_categorias').dataTable( {
	        "language": {
	        	"lengthMenu"	: "Mostrar _MENU_ por p&aacute;gina",
	            "zeroRecords"	: "No hay coincidencias",
	            "info"			: "Mostrando p&aacute;gina _PAGE_ de _PAGES_",
	            "infoEmpty"		: "No hay records disponibles",
	            "infoFiltered"	: "(filtrado de un total de _MAX_ registros)",
	            "sSearch"		: "Buscar:",
	            "sLoadingRecords": "Cargando...",
				"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "&Uacute;ltimo",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
							},
	        }
    	} );

		$("#tbl_categorias tbody" ).on('click', 'a.eliminar',  function()
		{
			var gotToURL = this.getAttribute('href');
			alertify.confirm("¿Está seguro que quiere borrar este record?", function (e) {
				if (e) {
				$.post(gotToURL, { accion : "delete" })
					.done(function(data, status,jqXHR)
					{
						if(data.error)
						{
							alertify.error(data.mensaje);
						}
						else
						{
							alertify.success(data.mensaje);
							window.setTimeout(function()
							{
								window.location.replace("{{ URL::action('AreasInvestigacionController@index')}}");
							},
							900);
						}
					})
					.fail(function(data, status,jqXHR)
					{
						console.log("Server Returned " + status);
						alertify.error("Error de comunicación con el servidor, contacte a su adminstrador.");
					});
				} else {return false;}
			
			});
			return false;
		});
		
		$("#tbl_categorias tbody" ).on('click', 'a.modificar',  function()
		{
			document.getElementById('lblCategoria').innerHTML = 'Modificar';
			
			var catID = this.getAttribute('id');
			lcName = document.getElementById('catName_'+catID).innerHTML;
			lcName = lcName.replace(/\n|\r/g, "").trim();
			document.getElementById('nomCategoria').value = lcName;
			document.getElementById('gotourl').value = this.getAttribute('href');
			document.getElementById('performAction').removeAttribute('disabled');
			document.getElementById('nomCategoria').removeAttribute('disabled');
			document.getElementById('nomCategoria').focus();
			return false;
		});
		
		$("#agregarCat").click(function()
		{
			document.getElementById('lblCategoria').innerHTML = 'Agregar';
			document.getElementById('gotourl').value = this.getAttribute('href');
			document.getElementById('performAction').removeAttribute('disabled');
			document.getElementById('nomCategoria').removeAttribute('disabled');
			document.getElementById('nomCategoria').value = '' ;
			document.getElementById('nomCategoria').focus();
			return false;
		});
		
		
		$("#performAction").click(function()
		{
			var nomCategoriaVal	= document.getElementById('nomCategoria').value.trim();
			var gotToURL		= document.getElementById('gotourl').value;
			if (checkFieldsEmpty('nomCategoria','Ingrese un valor para &Aacute;rea de Investigaci&oacute;n'))
				return;

			$.post(gotToURL, { accion : "edit", nomArea : nomCategoriaVal })
				.done(function(data, status,jqXHR)
				{
					if(data.error)
					{
						alertify.error(data.mensaje);
					}
					else
					{
						alertify.success(data.mensaje);
						window.setTimeout(function()
						{
							window.location.replace("{{ URL::action('AreasInvestigacionController@index')}}");
						},
						900);
					}
				})
				.fail(function(data, status,jqXHR)
				{
					console.log("Server Returned " + status);
					alertify.error("Error de comunicación con el servidor, contacte a su adminstrador");
				});
		});
		
		function checkFieldsEmpty(idControl,mensajeStop)
		{
			var detener = false;
			var controlHTML	= $("[name='"+idControl+"']").val().trim();
			if(controlHTML == '')
			{
				alertify.error(mensajeStop);
				$("[name='"+idControl+"']").parent().removeClass('has-error').addClass('has-error');
				detener = true;
			}else{$("[name='"+idControl+"']").parent().removeClass('has-error');}
			return detener;
		};
		
	});
</script>

@stop