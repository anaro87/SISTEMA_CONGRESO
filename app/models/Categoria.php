<?php

class Categoria extends Eloquent {

	protected $table = 'categoria';
	public $timestamps = false;
	protected $softDelete = false;
	protected $primaryKey = 'idCategoria';
	protected $key = 'idCategoria';

}