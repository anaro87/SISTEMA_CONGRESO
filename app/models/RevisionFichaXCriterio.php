<?php

class RevisionFichaXCriterio extends Eloquent {

	/**
	 *
	 * Nombre de la tabla en DB.
	 */
	protected $table = 'revision_ficha_x_criterio';

	/**
	 *
	 * llave primaria de la tabla en DB.
	*/
	protected $primaryKey = 'idRevisionFichaXCriteriO';

	public $timestamps = true;
	protected $softDelete = false;
	protected $fillable = ['idRevisionFicha','idPuntaje'];

}