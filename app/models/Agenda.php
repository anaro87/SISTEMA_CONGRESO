<?php

class Agenda extends Eloquent {

	protected $table = 'agenda';
	protected $primaryKey  = 'idAgenda'; //arodriguez
	public $timestamps = false;
	protected $softDelete = false;

}