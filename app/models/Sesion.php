<?php

class Sesion extends Eloquent {

	protected $table = 'sesion';
	protected $primaryKey  = 'idSesion'; //arodriguez
	public $timestamps = false;
	protected $softDelete = false;
	protected $fillable = ['idSesion'];
}