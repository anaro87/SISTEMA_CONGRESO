<?php

class CriterioEvaluacion extends Eloquent {

	protected $table = 'criterio_evaluacion';
	protected $primaryKey  = 'idCriterioEvaluacion';
	protected $fillable = ['nombreCriterio', 'idDetalleCongreso'];
	public $timestamps = true;
	protected $softDelete = false;

}