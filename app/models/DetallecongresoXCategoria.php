<?php

class DetallecongresoXCategoria extends Eloquent {

	protected $table = 'detallecongreso_x_categoria';
	protected $fillable = ['idCategoria','idDetalleCongreso'];
	public $timestamps = true;
	protected $softDelete = false;

}