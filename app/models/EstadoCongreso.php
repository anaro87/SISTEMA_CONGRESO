<?php

class EstadoCongreso extends Eloquent {

	protected $table = 'estado_congreso';
	public $timestamps = false;
	protected $softDelete = false;

}