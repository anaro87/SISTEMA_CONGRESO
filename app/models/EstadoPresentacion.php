<?php

class EstadoPresentacion extends Eloquent {

	protected $table = 'estado_presentacion';
	protected $primaryKey  = 'idEstadoPresentacion'; //arodriguez
	public $timestamps = true;
	protected $softDelete = false;

}